from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import Image
from kivy.clock import Clock
from kivy.graphics.texture import Texture
# from android.permissions import request_permissions, Permission
import cv2
import numpy as np


class CamApp(App):

    def build(self):
        # request_permissions([
        #     Permission.CAMERA,
        #     Permission.WRITE_EXTERNAL_STORAGE,
        #     Permission.READ_EXTERNAL_STORAGE
        # ])

        # inicializacion de parametros
        self.parametros = cv2.aruco.DetectorParameters_create()
        # cargamos diccionario aruco
        self.diccionario = cv2.aruco.Dictionary_get(cv2.aruco.DICT_5X5_100)

        self.img1 = Image()
        layout = BoxLayout()
        layout.add_widget(self.img1)
        # opencv2 stuffs
        self.capture = cv2.VideoCapture(0)
        Clock.schedule_interval(self.update, 1.0 / 33.0)
        return layout

    def update(self, dt):
        # display image from cam in opencv window
        ret, frame = self.capture.read()
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)  # DETECTOR ARUCOS ESCALA DE GRISES
        esquinas, ids, candidatos_malos = cv2.aruco.detectMarkers(gray, self.diccionario, parameters=self.parametros)

        if np.all(ids != None):
            aruco = cv2.aruco.drawDetectedMarkers(frame, esquinas)

            # coordenadas
            c1 = (esquinas[0][0][0][0], esquinas[0][0][0][1])
            c2 = (esquinas[0][0][1][0], esquinas[0][0][1][1])
            c3 = (esquinas[0][0][2][0], esquinas[0][0][2][1])
            c4 = (esquinas[0][0][3][0], esquinas[0][0][3][1])

            copy = frame
            # leemos imagen a sobreponer

            imagen = cv2.imread("s2.png")
            # estraemos el tamaño de la imagen
            tamaño = imagen.shape
            # acomodar coordenadas del aruco
            puntos_aruco = np.array([c1, c2, c3, c4])

            # organizacion de las cooredenas de la imagen

            puntos_imagen = np.array([
                [0, 0],
                [tamaño[1] - 1, 0],
                [tamaño[1] - 1, tamaño[0] - 1],
                [0, tamaño[0] - 1]
            ], dtype=float)

            # superpocision de imagenes
            h, estado = cv2.findHomography(puntos_imagen, puntos_aruco)

            # realizamos la transformacion de la perspectiva
            perspectiva = cv2.warpPerspective(imagen, h, (copy.shape[1], copy.shape[0]))
            cv2.fillConvexPoly(copy, puntos_aruco.astype(int), 0, 16)
            copy = copy + perspectiva
            frame = copy

        # convert it to texture
        buf1 = cv2.flip(frame, 0)
        buf = buf1.tostring()
        texture1 = Texture.create(size=(frame.shape[1], frame.shape[0]), colorfmt='bgr')
        # if working on RASPBERRY PI, use colorfmt='rgba' here instead, but stick with "bgr" in blit_buffer.
        texture1.blit_buffer(buf, colorfmt='bgr', bufferfmt='ubyte')
        # display image from the texture
        self.img1.texture = texture1


if __name__ == '__main__':
    CamApp().run()

![LOGO](img/visitar-logo.jpeg)

## _Aplicación móvil de recorrido con realidad aumentada que pueda ser usada en las instalaciones del planetario luis enrique erro_

### Programas necesarios para el desarrollo

Para poder ejecutar la aplicación de python y crear el APK se necesitarán las siguientes herramientas.

- Android studio ([Main page](https://developer.android.com/studio), [Snapd](https://snapcraft.io/android-studio))
- VSCode ([Main page](https://code.visualstudio.com/), [Snapd](https://snapcraft.io/code))
- PyCharm ([Main page](https://www.jetbrains.com/pycharm/download/), [Snapd](https://snapcraft.io/pycharm-professional))
- JDK 8 ([Oracle](https://www.oracle.com/mx/java/technologies/javase/javase8-archive-downloads.html), [OpenJDK](https://docs.datastax.com/en/jdk-install/doc/jdk-install/installOpenJdkDeb.html))
- Ubuntu 20.04 (Recomendado)

### Comandos previos a la configuración

Antes de realizar la configuración del entorno se deberán ejecutar los siguientes comandos para instalar herramientas necesarias para paquetes que se instalaran más adelante.

```shell
sudo dpkg --add-architecture i386
sudo apt-get update
sudo apt-get install lld
sudo apt install -y git zip unzip openjdk-8-jdk python3-pip autoconf libtool pkg-config zlib1g-dev libncurses5-dev libncursesw5-dev libtinfo5 cmake libffi-dev libssl-dev
sudo apt-get install -y build-essential ccache git zlib1g-dev python3 python3-dev libncurses5:i386 libstdc++6:i386 zlib1g:i386 openjdk-8-jdk unzip ant ccache autoconf libtool libssl-dev
```

### Configuración de Android Studio

Para construir el APK de nuestra aplicación será necesario descargar desde android studio el SDK y el NDK de la siguiente forma

Al abrir Android studio se mostrará la siguiente ventana, en la cual haremos click en _More Actions_ y despues seleccionaremos la opcion de SDK manager

![Android_studio_settings](img/1_1.png)

Cuando entremos a la ventana del SDK manager seleccionaremos las siguientes versiones del SDK Platforms, se seleccionan a partir de Android 6.0 únicamente para realizar pruebas

![Android_studio_settings](img/1_2.png)

Después en la pestaña de SDK Tools seleccionaremos todas las opciones del SDK Build-Tools solo para realizar pruebas

![Android_studio_settings](img/1_3.png)

Y para finalizar seleccionaremos las siguientes versiones del NDK.

![Android_studio_settings](img/1_4.png)

### Variables de entorno

Antes de realizar la instalación de los paquetes de Python y una vez que ya se tenga configurado Java y Android Studio se tendrán que agregar las siguientes variables de entorno a nuestro archivo ~/.bashrc o ~./.zshrc dependiendo de que shell se este utilizando

```shell
# JAVA PATH
export JAVA_HOME="/usr/lib/jvm/java-8-openjdk-amd64/bin/"

# ANDROID STUDIO TOOLS PATH
export ANDROID_HOME="/home/gerardo/Android/Sdk"
export PATH=$PATH:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools

# BUILDOZER / PYTHON FOR ANDROID ENVIRONMENT VARIABLES
export ANDROIDSDK=$ANDROID_HOME
export ANDROIDNDK=$ANDROID_HOME/ndk/21.1.6352462
export ANDROIDAPI="26"
export NDKAPI="21"
```

### Comandos para Ubuntu

Para configurar el entorno de desarrollo en Python se utilizara la versión 3.6 y los comandos necesarios para la instalación son los siguientes

```sh
sudo apt install software-properties-common
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt install python3.6
```

Para establecer la versión de Python por default se deberá usar los siguientes comandos

```sh
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3.6 1
sudo update-alternatives --config python #Seleccionar la version 3.6
```

Adicionalmente se instalará el gestor de paquetes pip para descargar los paquetes necesarios

```sh
sudo apt-get install python3-pip
```

### Entorno virtual para Python
Una vez instalado pip procederemos a crear un entorno virtual de Python para instalar nuestros paquetes de Python utilizando PyCharm

Al abrir _Settings_ vamos a escribir _Interpreter_ en la barra de búsqueda y seleccionaremos la opción que se encuentra bajo nuestro proyecto como se muestra en la imagen

![Interpreter_addition](img/1.png)

Al hacer click en el menú contextual de Python Interpreter seleccionaremos la opción de _Show All_

![Interpreter_addition](img/2.png)

En la siguiente ventana seleccionaremos el símbolo de +

![Interpreter_addition](img/3.png)

Y finalmente nos mostrará la siguiente ventana en la que podremos seleccionar el nombre de nuestro entorno virtual cambiando el nombre seleccionado en la imagen, además es importante seleccionar la versión de Python que se instaló al inicio (Python 3.6)

![Interpreter_addition](img/4.png)

Una vez que tengamos nuestro entorno virtual, al abrir la terminal de PyCharm nos mostrará el prompt de la siguiente forma.
Indicando que se encuentra activo el entorno virtual new_interpreter
```sh
(new_interpreter) /home/gerardo/Documents/TT1
```

### Paquetes necesarios para el desarrollo

Una vez que se tenga nuestro entorno virtual podremos instalar los paquetes necesarios con los siguientes comandos

```sh
pip install python-for-android      # Build APK
pip install kivy kivy_examples      # Build interface
pip install opencv-contrib-python   # Use Aruco and create connection with the camera
pip install cython                  # Required for APK's building
pip install buildozer               # Build APK
```

## Listo, ya tienes tu entorno configurado para trabajar con visitAR
